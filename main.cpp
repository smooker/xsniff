#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

struct MwmHints {
    unsigned long flags;
    unsigned long functions;
    unsigned long decorations;
    long input_mode;
    unsigned long status;
};
enum {
    MWM_HINTS_FUNCTIONS = (1L << 0),
    MWM_HINTS_DECORATIONS =  (1L << 1),

    MWM_FUNC_ALL = (1L << 0),
    MWM_FUNC_RESIZE = (1L << 1),
    MWM_FUNC_MOVE = (1L << 2),
    MWM_FUNC_MINIMIZE = (1L << 3),
    MWM_FUNC_MAXIMIZE = (1L << 4),
    MWM_FUNC_CLOSE = (1L << 5)
};


int main()
{
                Display* display = XOpenDisplay(NULL);
                XSetWindowAttributes attributes;
//                attributes.event_mask = SubstructureNotifyMask | StructureNotifyMask  ;
                attributes.event_mask = SubstructureNotifyMask | VisibilityChangeMask | EnterWindowMask;
                attributes.backing_store = Always;
                Window win = XDefaultRootWindow(display);
                printf("Deafult root window %lu\n", win);
                XChangeWindowAttributes(display, win, CWEventMask, &attributes);

                while (1)
                {
                   XEvent event;
                   XNextEvent(display, &event);

                   if (event.type == MotionNotify) {
                       printf("motion : Window root: %lu\n", event.xmotion.root);
                       printf("motion 2: %lu\n", event.xmotion.window);

                   }

                   if (event.type == CreateNotify)
                   {
//                         puts("create Notify event occured\n");
                        printf("create Window ID: %lu\n", event.xcreatewindow.window);
//                        printf("Window ID: %d\n", event.xcreatewindow.window);
                        if (event.xcreatewindow.width == 640 && event.xcreatewindow.height == 360) {
                            printf("NIE SME\n");
                            //nie sme
                            Atom mwmHintsProperty = XInternAtom(event.xcreatewindow.display, "_MOTIF_WM_HINTS", 0);
                            struct MwmHints hints;
                            hints.flags = MWM_HINTS_DECORATIONS;
                            hints.decorations = 0;
                            XChangeProperty(event.xcreatewindow.display, event.xcreatewindow.window, mwmHintsProperty, mwmHintsProperty, 32,
                                    PropModeReplace, (unsigned char *)&hints, 5);

                        }
//                        printf("Parent Window ID: %x\n", event.xcreatewindow.parent);
                  }
                }
}
