#!/usr/bin/perl
use DBD::mysql;
use strict;
use warnings;
use IPC::Open3;
use Getopt::Long;
use Proc::ProcessTable;


GetOptions(
    "host=s" => \my $dbhost,
    "name=s" => \my $dbname,
    "user=s" => \my $dbuser,
    "pass=s" => \my $dbpass,
);

my $dbh = DBI->connect("DBI:mysql:database=$dbname;host=$dbhost;mysql_connect_timeout=1",
                         "$dbuser", "$dbpass",
                         {'RaiseError' => 1});

sub killall {
    my $signal=15;
    my $parent=shift; # Parent PID from args
    my $proc_table=Proc::ProcessTable->new();
    foreach my $proc (@{$proc_table->table()}) {
      kill($signal, $proc->pid) if ($proc->ppid == $parent);
    }
}

sub getLastActiveTerminals {
        my %retHash = ();
        my $fieldNum = 0;
        my %retHash2 = ();

#        my $sth = $dbh->prepare("SELECT * FROM terminals WHERE ter_id<>332 ORDER BY lu_ts DESC LIMIT 0, 9;");
        my $sth = $dbh->prepare("SELECT \@a:=\@a+1 AS vgz, te.* FROM terminals te, (SELECT \@a:=0) te2 WHERE ter_id<>332 ORDER BY lu_ts DESC LIMIT 0, 9;");
        $sth->execute();
        while (my $ref = $sth->fetchrow_hashref()) {
                $retHash{$fieldNum} = $ref->{'ip'};
                $retHash2{$fieldNum++} = $ref->{'vgz'};
        }
        return (\%retHash, \%retHash2);
}

#        my %fieldNames = getFieldNames($table);


my $p = 0;

system("killall ssvncviewer");
system("killall xsniff");
system("./xsniff &");

my %showTerminals;

my($v1, $v2) = getLastActiveTerminals();


%showTerminals = %$v1;

my %pids;       #PIDs
my %iporder = %$v2;
my %wtr;        #stdout
my %rdr;        #stdin
my %err;        #stderr

sub showVNCs {
    #
    my $p = 0;
    for(my $y=0; $y<1080; $y+=360) {
            for(my $i=0; $i<1920; $i+=640) {
                    my $ert = sprintf("%s", $showTerminals{$p});
    #		print $p." - ".$terms[$p]."\n";
                    $pids{$ert} = open3($wtr{$ert}, $rdr{$ert}, $err{$ert}, "SSVNC_SCALE=640x360 ssvncviewer -viewonly -compresslevel 0 -geometry 640x360+".$i."+".$y." -passwd ~/.vnc/passwd ".$ert." ");
                    $p++;
            }
    }
}

showVNCs();

while (1) {
    my($v1, $v2) = getLastActiveTerminals();
    my %currActiveTerminals = %$v1;

    foreach my $field1 (sort{$a <=> $b}(keys %showTerminals)) {
            print "#field1::".$showTerminals{$field1}."\n";
            if ($currActiveTerminals{$field1} ne $showTerminals{$field1}) {
                #imame nov terminal
                print "ALELELEE:".$field1."\n";
                delete $showTerminals{$field1};     #nenuzhno...
                #miastoto mu e na nai-staria.. demek nomer 9
                foreach my $pid2 (values %pids) {
                    killall($pid2);
                }
                undef %pids;
                undef %iporder;
                undef %wtr;
                undef %rdr;
                undef %err;

                ($v1, $v2) = getLastActiveTerminals();
                %showTerminals = %$v1;

                showVNCs();
            }
    }

    sleep(3);

    foreach my $ip(keys %pids) {
        print "IP:".$ip."\tPID:".$pids{$ip}."\n";
    }
#    killall($pids{'192.168.112.115'});
#    kill -9, $pids{'192.168.112.115'};

#    print "after kill\n$pids{'192.168.112.115'}\n";

    sleep(3);
}

print "END"."\n";
